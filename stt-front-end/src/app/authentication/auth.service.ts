import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import 'rxjs/add/operator/filter';
import * as auth0 from 'auth0-js';

@Injectable()
export class AuthService {

  account: any;

  auth0 = new auth0.WebAuth({
    clientID: 'OKJ0jVawNIF0jRGMlNeG2kiOYQCPRhPS',
    domain: 'stt.eu.auth0.com',
    responseType: 'token id_token',
    audience: 'https://bitbucket.org/stt_ntnu/stt_acsp/',
    redirectUri: 'http://localhost:4200/callback',
    scope: 'openid profile'
  });

  constructor(public router: Router) { }

  /**
   * 
   */
  public login(): void {
    this.auth0.authorize();
  }
  
  /**
   * 
   */
  public handleAuthentication(): void {
    this.auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        window.location.hash = '';
        this.setSession(authResult);
        this.router.navigate(['/home']);
      } else if (err) {
        this.router.navigate(['/home']);
        console.log(err);
      }
    });
  }

  /**
   * After a succesful login this method stores the user's login response in local storage.
   * @param authResult The result from the login dialogue.
   */
  private setSession(authResult): void {
    const expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
    localStorage.setItem('access_token', authResult.accessToken);
    localStorage.setItem('id_token', authResult.idToken);
    localStorage.setItem('expires_at', expiresAt);
  }

  /**
   * Clears authentication information from local storage, and redirects to homepage.
   */
  public logout(): void {
    localStorage.removeItem('access_token');
    localStorage.removeItem('id_token');
    localStorage.removeItem('expires_at');
    this.router.navigate(['/']);
  }

  /**
   * Checks if the token the user obtained during login is still valid.
   */
  public isAuthenticated(): boolean {
    const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
    return new Date().getTime() < expiresAt;
  }

  public getAccount(callback): object {
    const accessToken = localStorage.getItem('access_token');
    if(!accessToken){
      return {};
      // throw new Error('Please make sure that the access token exists, before accessing user info');
    }

    this.auth0.client.userInfo(accessToken, (err, profile) => {
      if(profile){
        this.account = profile;
      }
      callback(err, profile);
    });
  }



}
