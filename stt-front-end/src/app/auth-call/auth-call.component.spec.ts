import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthCallComponent } from './auth-call.component';
import { AuthService}  from '../authentication/auth.service';
import { RouterTestingModule } from '@angular/router/testing';

import { Http, RequestOptions } from '@angular/http';
import { AuthHttp, AuthConfig } from 'angular2-jwt';

export function authHttpServiceFactory(http: Http, options: RequestOptions){
  return new AuthHttp(new AuthConfig({
    tokenGetter: (() => localStorage.getItem('access_token'))
  }), http, options)
}


describe('AuthCallComponent', () => {
  let component: AuthCallComponent;
  let fixture: ComponentFixture<AuthCallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule], 
      declarations: [ AuthCallComponent ],
      providers: [
        Http,
        AuthService,
        {
          provide: AuthHttp,
          useFactory: authHttpServiceFactory,
          deps: [Http, RequestOptions]
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthCallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
