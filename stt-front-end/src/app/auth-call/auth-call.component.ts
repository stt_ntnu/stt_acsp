import { Component, OnInit } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
//import { Http, XHRBackend, RequestOptions, Request, RequestOptionsArgs, Response, Headers } from '@angular/http';

import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';


export interface ItemsResponse {
  results: string[];
};

@Component({
  selector: 'app-auth-call',
  templateUrl: './auth-call.component.html',
  styleUrls: ['./auth-call.component.css']
})


export class AuthCallComponent implements OnInit {
  
  message: string = '<call result appears here>';

  id_token_msg: string = "";
  id_token: string  = ""; 
  access_token_msg: string = "";
  access_token: string = "";
  count = 0;
  constructor(private authHttp: AuthHttp, private http: HttpClient) { }

  public toogleTokens(): void{
    if(this.count === 0){
      this.access_token_msg = "Access token:";
      this.id_token_msg = "Id Token:";
      this.id_token = localStorage.getItem('id_token');
      this.access_token = localStorage.getItem('access_token');
      this.count = 1;
    }else{
      this.access_token_msg = "";
      this.id_token_msg = "";
      this.id_token = "";
      this.access_token = "";
      this.count = 0;
    }
    
  }

  public testPublicCall(): void {
    var api_url = environment.api_url + "api/public";
    this.http.get(api_url)
      .subscribe(
        data => this.message = JSON.stringify(data),
        error => this.message = error
      );
  }

  public testPrivateCall(location): void {
    var api_url = environment.api_url + location;
    this.authHttp.get(api_url)
      .map(res => res.json())
      .subscribe(
        data => this.message = data.message,
        error => this.message = error
      );
  }

  public privateCall(){
    this.testPrivateCall('api/private/');
  }

  public adminCall(){
    this.testPrivateCall('api/private/admin');
  }

  public teacherCall(){
    this.testPrivateCall('api/private/teacher');
  }

  public studentCall(){
    this.testPrivateCall('api/private/student');
  }

  public userCall(){
    this.testPrivateCall('api/private/user');
  }

  ngOnInit() {
  }

}
