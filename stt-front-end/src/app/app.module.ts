import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {HttpClientModule} from '@angular/common/http';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { TagInputModule } from 'ngx-chips';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

import { BasisPathTestingComponent } from './basis-path-testing/basis-path-testing.component';
import { BasisPathPage2Component } from './basis-path-testing/basis-path-page2/basis-path-page2.component';
import { BasisPathPage3Component } from './basis-path-testing/basis-path-page3/basis-path-page3.component';
import { BasisPathPage4Component } from './basis-path-testing/basis-path-page4/basis-path-page4.component';
import { BasisPathPage5Component } from './basis-path-testing/basis-path-page5/basis-path-page5.component';


import { DataFlowTestingComponent } from './data-flow-testing/data-flow-testing.component';
import { DataFlowP2Component } from './data-flow-testing/data-flow-p2/data-flow-p2.component';

import { QuizComponent } from './quiz/quiz.component';

import { AccountComponent } from './account/account.component';
import { LogoutComponent } from './logout/logout.component';
import { AuthCallComponent } from './auth-call/auth-call.component';

import { AuthService } from './authentication/auth.service';
import { QuizService } from './quiz-service/quiz.service';

import { Http, RequestOptions } from '@angular/http';
import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { ForumComponent } from './forum/forum.component';
import { DiscussionComponent } from './forum/discussion/discussion.component';
import { AdminQuizComponent } from './admin-quiz/admin-quiz.component';
import { QuestionsComponent } from './admin-quiz/questions/questions.component';
import { AddQuizComponent } from './admin-quiz/add-quiz/add-quiz.component';
import { AddQuestionComponent } from './admin-quiz/add-question/add-question.component';
import { BasisPathPage6Component } from './basis-path-testing/basis-path-page6/basis-path-page6.component';
import { BasisPathPage7Component } from './basis-path-testing/basis-path-page7/basis-path-page7.component';
import { BasisPathPage8Component } from './basis-path-testing/basis-path-page8/basis-path-page8.component';
import { BasisPathPage9Component } from './basis-path-testing/basis-path-page9/basis-path-page9.component';
import { BasisPathPage10Component } from './basis-path-testing/basis-path-page10/basis-path-page10.component';
import { BasisPathPage11Component } from './basis-path-testing/basis-path-page11/basis-path-page11.component';
import { BasisPathPage12Component } from './basis-path-testing/basis-path-page12/basis-path-page12.component';
import { BasisPathPage13Component } from './basis-path-testing/basis-path-page13/basis-path-page13.component';
/*
import { BasisquizComponent } from './basisquiz/basisquiz.component';
*/
import { DataFlowP3Component } from './data-flow-testing/data-flow-p3/data-flow-p3.component';
import { DataFlowP4Component } from './data-flow-testing/data-flow-p4/data-flow-p4.component';
import { DataFlowP5Component } from './data-flow-testing/data-flow-p5/data-flow-p5.component';
import { DataFlowP6Component } from './data-flow-testing/data-flow-p6/data-flow-p6.component';
import { DataFlowP7Component } from './data-flow-testing/data-flow-p7/data-flow-p7.component';

export function authHttpServiceFactory(http: Http, options: RequestOptions){
  return new AuthHttp(new AuthConfig({
    tokenGetter: (() => localStorage.getItem('access_token'))
  }), http, options)
}

@NgModule({
  declarations: [
    AppComponent,
    DataFlowTestingComponent,
    BasisPathTestingComponent,
    HomeComponent,
    AccountComponent,
    BasisPathPage2Component,
    BasisPathPage3Component,
    DataFlowP2Component,
    LogoutComponent,
    AuthCallComponent,
    BasisPathPage4Component,
    BasisPathPage5Component,
    QuizComponent,
    ForumComponent,
    DiscussionComponent,
    AdminQuizComponent,
    QuestionsComponent,
    AddQuizComponent,
    AddQuestionComponent,
    BasisPathPage6Component,
    BasisPathPage7Component,
    BasisPathPage8Component,
    BasisPathPage9Component,
    BasisPathPage10Component,
    BasisPathPage11Component,
    BasisPathPage12Component,
    BasisPathPage13Component,
   // BasisquizComponent
    DataFlowP3Component,
    DataFlowP4Component,
    DataFlowP5Component,
    DataFlowP6Component,
    DataFlowP7Component
  ],
  imports: [
    TagInputModule,
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    MultiselectDropdownModule,
    RouterModule.forRoot([
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      },
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'dataflow',
        children: [
                  {
                    path: '',
                    component: DataFlowTestingComponent
                  },
                  {
                    path: '2',
                    component: DataFlowP2Component
                  },
                  {
                    path: '3',
                    component: DataFlowP3Component
                  },
                  {
                    path: '4',
                    component: DataFlowP4Component
                  },
                  {
                    path: '5',
                    component: DataFlowP5Component
                  },
                  {
                    path: '6',
                    component: DataFlowP6Component
                  },
                  {
                    path: '7',
                    component: DataFlowP7Component
                  }
        ]
      },
      {
        path: 'quiz',
        component: QuizComponent
      },
      {
        path: 'basispath',
        children: [
                  {
                    path: '',
                    component: BasisPathTestingComponent
                  },
                  {
                    path: '2',
                    component: BasisPathPage2Component
                  },
                  {
                    path: '3',
                    component: BasisPathPage3Component
                  },
                  {
                    path: '4',
                    component: BasisPathPage4Component
                  },
                  {
                    path: '5',
                    component: BasisPathPage5Component
                  },
                  {
                    path: '6',
                    component: BasisPathPage6Component
                  },

                  {
                    path: '7',
                    component: BasisPathPage7Component
                  },
                  {
                    path: '8',
                    component: BasisPathPage8Component
                  },
                  {
                    path: '9',
                    component: BasisPathPage9Component
                  },

                  {
                    path: '10',
                    component: BasisPathPage10Component
                  },
                  {
                    path: '11',
                    component: BasisPathPage11Component
                  },
                  {
                    path: '12',
                    component: BasisPathPage12Component
                  },
                  {
                    path: '13',
                    component: BasisPathPage13Component
                  }

        ]
      },
      {
        path: 'account',
        component: AccountComponent
      },
      {
        path: 'callback',
        redirectTo: 'home',
        pathMatch: 'full'
      },
      {
        path: 'test-call',
        component: AuthCallComponent
      },
      {
        path: 'forum',
        component: ForumComponent
      },
      {
        path: 'forum/:id/discussion',
        component: DiscussionComponent
      },
      {
        path: 'admin-quiz',
        children: [
                  {
                    path: '',
                    component: AdminQuizComponent
                  },
                  {
                    path: ':id/questions',
                    component: QuestionsComponent
                  },
                  {
                    path: 'add',
                    component: AddQuizComponent
                  },
                  {
                    path: ':id/questions/add',
                    component: AddQuestionComponent
                  }
        ]
      }
    ])

  ],
  providers: [
    AuthService,
    QuizService,
    {
      provide: AuthHttp,
      useFactory: authHttpServiceFactory,
      deps: [Http, RequestOptions]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
