import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { AuthHttp } from 'angular2-jwt';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';


@Injectable()
export class QuizService {

	// private url = 'localhost:3000/'
  constructor(private authHttp: AuthHttp, private http: HttpClient) { }

  public testPrivateCall(location): void {
    var api_url = environment.api_url + location;
    this.authHttp.get(api_url)
      .map(res => res.json())
      .subscribe(res=>{
              console.log(res);
            });
  }

  public postQuiz(quizData): void {
    var api_url = environment.api_url + 'questionnaire/quiz';
    this.authHttp.post(api_url, quizData)
      .map(res => res.json())
      .subscribe(res=>{
              console.log(res);
            },
            (err: HttpErrorResponse)=>{console.log(err.error.message)});
  }

}
