import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataFlowP6Component } from './data-flow-p6.component';

describe('DataFlowP6Component', () => {
  let component: DataFlowP6Component;
  let fixture: ComponentFixture<DataFlowP6Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataFlowP6Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataFlowP6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
