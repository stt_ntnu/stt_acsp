import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-flow-p6',
  templateUrl: './data-flow-p6.component.html',
  styleUrls: ['./data-flow-p6.component.css']
})
export class DataFlowP6Component implements OnInit {

  step:number = 1;
  selected:boolean [] = [false, false, false, false, false, false];
  comments:string [] = ["    //definition of r", "    //use of r"];
  activeComments:string [] = [" ", " ", " ", " ", " ", " "];

  definition:number = 0;
  use:number = 0;
  progress:number = 0;

  createdDefUses:DefUse [] = [];
  defUses:DefUse [] = [
    {
      def:4,
      use:6,
      created:false,
      valid:true,
      comment:"Correct! [4-6] is a valid def-use pair."
    },
    {
      def:4,
      use:9,
      created:false,
      valid:false,
      comment:"Try again! [4-9] does not have a def-clear path (r is re-defined on line 9)"
    },
    {
      def:4,
      use:11,
      created:false,
      valid:false,
      comment:"Try again! [4-11] does not have a def-clear path (r is re-defined on line 9)"
    },
    {
      def:8,
      use:6,
      created:false,
      valid:false,
      comment:"Try again! [8-6] is not a valid def-use pair (We can not go backwards in the code)"
    },
    {
      def:8,
      use:9,
      created:false,
      valid:true,
      comment:"Correct! [8-9] is a valid def-use pair."
    },
    {
      def:8,
      use:11,
      created:false,
      valid:true,
      comment:"Correct! [8-11] is a valid def-use pair."
    },
    {
      def:12,
      use:6,
      created:false,
      valid:false,
      comment:"Try again! [12-6] is not a valid def-use pair (We can not go backwards in the code)"
    },
    {
      def:12,
      use:9,
      created:false,
      valid:true,
      comment:"Correct! [12-9] is a valid def-use pair."
    },
    {
      def:12,
      use:11,
      created:false,
      valid:true,
      comment:"Correct! [12-11] is a valid def-use pair."
    }
  ]

  constructor() { }

  ngOnInit() {
  }

  nextStep(currentStep:number, id:number) {
    if(this.step==currentStep && this.selected[id]==false)
    {
      this.activeComments[id] = this.comments[currentStep-1];
      this.selected[id] = true;
      if(this.selected[0] && this.selected[1] && this.selected[2]){
        this.step = 2;
      }
      if(this.selected[3] && this.selected[4] && this.selected[5]){
        this.step = 3;
      }
    }
  }

  selectDefinition(id:number) {
    this.definition=id;
    if(this.use>0){
      this.checkDefUse(this.definition, this.use);
    }
  }
  selectUse(id:number) {
    this.use=id;
    if(this.definition>0){
      this.checkDefUse(this.definition, this.use);
    }
  }
  checkDefUse(def:number, use:number) {
    for (let defUse of this.defUses){
      if(def==defUse.def && use==defUse.use && defUse.created==false) {
        defUse.created=true;
        this.createdDefUses.push(defUse);
        if(defUse.valid){
          this.progress++;
          this.use = 0;
          this.definition = 0;
        }
        if(this.progress>=5) {
          this.step = 4;
        }
      }
    }
  }
}

export interface DefUse {
  def,use:number;
  created,valid:boolean;
  comment:string;
}