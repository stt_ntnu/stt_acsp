import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataFlowP7Component } from './data-flow-p7.component';

describe('DataFlowP7Component', () => {
  let component: DataFlowP7Component;
  let fixture: ComponentFixture<DataFlowP7Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataFlowP7Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataFlowP7Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
