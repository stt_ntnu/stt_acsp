import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataFlowP4Component } from './data-flow-p4.component';

describe('DataFlowP4Component', () => {
  let component: DataFlowP4Component;
  let fixture: ComponentFixture<DataFlowP4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataFlowP4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataFlowP4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
