import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-flow-p4',
  templateUrl: './data-flow-p4.component.html',
  styleUrls: ['./data-flow-p4.component.css']
})
export class DataFlowP4Component implements OnInit {

  step:number = 1;
  comments:string [] = ["    //definition of n", "    //use of n"]
  activeComments:string [] = [" ", " "];
  
  constructor() { }

  ngOnInit() {
  }

  nextStep(current:number) {
    if(this.step==current)
    {
      this.activeComments[current-1] = this.comments[current-1];
      this.step++;
    }
  }


}
