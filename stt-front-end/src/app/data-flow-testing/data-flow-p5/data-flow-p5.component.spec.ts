import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataFlowP5Component } from './data-flow-p5.component';

describe('DataFlowP5Component', () => {
  let component: DataFlowP5Component;
  let fixture: ComponentFixture<DataFlowP5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataFlowP5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataFlowP5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
