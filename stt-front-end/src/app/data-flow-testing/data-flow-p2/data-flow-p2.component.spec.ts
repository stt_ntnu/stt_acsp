import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataFlowP2Component } from './data-flow-p2.component';

describe('DataFlowP2Component', () => {
  let component: DataFlowP2Component;
  let fixture: ComponentFixture<DataFlowP2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataFlowP2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataFlowP2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
