import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataFlowP3Component } from './data-flow-p3.component';

describe('DataFlowP3Component', () => {
  let component: DataFlowP3Component;
  let fixture: ComponentFixture<DataFlowP3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataFlowP3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataFlowP3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
