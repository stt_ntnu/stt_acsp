import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataFlowTestingComponent } from './data-flow-testing.component';
import { RouterTestingModule } from '@angular/router/testing'

describe('DataFlowTestingComponent', () => {
  let component: DataFlowTestingComponent;
  let fixture: ComponentFixture<DataFlowTestingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule ],
      declarations: [ DataFlowTestingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataFlowTestingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
