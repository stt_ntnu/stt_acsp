import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { AuthService}  from './authentication/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  constructor(public auth: AuthService, public router: Router) {
    auth.handleAuthentication();
  }

  account;
  public isTeacher = false;

  ngOnInit(){
    this.router.events.subscribe((evt) => {
        if (!(evt instanceof NavigationEnd)) {
            return;
        }
        window.scrollTo(0, 0)
      
        var serviceAccount = this.auth.account;
        if(serviceAccount){
          this.account = serviceAccount;
        } else{
          this.auth.getAccount((err, profile) => {
            this.account = profile;
            for(var role in this.account['https://stt.ntnu.no/roles']){
              if(this.account['https://stt.ntnu.no/roles'][role] == "admin" || this.account['https://stt.ntnu.no/roles'][role] == "teacher"){
                this.isTeacher = true;
              }
            }
          });
        }
    });
    
  }


}
