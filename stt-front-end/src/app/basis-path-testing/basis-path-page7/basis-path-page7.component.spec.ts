import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasisPathPage7Component } from './basis-path-page7.component';

describe('BasisPathPage7Component', () => {
  let component: BasisPathPage7Component;
  let fixture: ComponentFixture<BasisPathPage7Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasisPathPage7Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasisPathPage7Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
