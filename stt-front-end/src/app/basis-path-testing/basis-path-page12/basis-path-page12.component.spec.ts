import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasisPathPage12Component } from './basis-path-page12.component';

describe('BasisPathPage12Component', () => {
  let component: BasisPathPage12Component;
  let fixture: ComponentFixture<BasisPathPage12Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasisPathPage12Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasisPathPage12Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
