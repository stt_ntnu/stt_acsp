import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasisPathPage10Component } from './basis-path-page10.component';

describe('BasisPathPage10Component', () => {
  let component: BasisPathPage10Component;
  let fixture: ComponentFixture<BasisPathPage10Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasisPathPage10Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasisPathPage10Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
