import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasisPathPage9Component } from './basis-path-page9.component';

describe('BasisPathPage9Component', () => {
  let component: BasisPathPage9Component;
  let fixture: ComponentFixture<BasisPathPage9Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasisPathPage9Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasisPathPage9Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
