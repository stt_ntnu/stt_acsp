import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasisPathPage8Component } from './basis-path-page8.component';

describe('BasisPathPage8Component', () => {
  let component: BasisPathPage8Component;
  let fixture: ComponentFixture<BasisPathPage8Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasisPathPage8Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasisPathPage8Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
