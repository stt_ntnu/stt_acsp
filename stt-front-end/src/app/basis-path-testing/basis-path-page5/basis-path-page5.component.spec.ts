import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasisPathPage5Component } from './basis-path-page5.component';

describe('BasisPathPage5Component', () => {
  let component: BasisPathPage5Component;
  let fixture: ComponentFixture<BasisPathPage5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasisPathPage5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasisPathPage5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
