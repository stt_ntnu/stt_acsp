import { Component, OnInit } from '@angular/core';

import * as $ from 'jquery';
// declare var flowchart: any;
// declare var vis:any;

@Component({
  selector: 'app-basis-path-page4',
  templateUrl: './basis-path-page4.component.html',
  styleUrls: ['./basis-path-page4.component.css']
})
export class BasisPathPage4Component implements OnInit {

  constructor() { }

  ngOnInit() {

    // create an array with nodes
    // var nodes = new vis.DataSet([
    //   {id: 1, label: 'Start'},
    //   {id: 2, label: 'Node 1'},
    //   {id: 3, label: 'Node 2'},
    //   {id: 4, label: 'Node 3'},
    //   {id: 5, label: 'Node 4'},
    //   {id: 6, label: 'End'}
    // ]);

    // // create an array with edges
    // var edges = new vis.DataSet([
    //   {from: 1, to: 2},
    //   {from: 2, to: 3},
    //   {from: 3, to: 4},
    //   {from: 4, to: 3},
    //   {from: 3, to: 5},
    //   {from: 5, to: 6}
    // ]);

    // // create a network
    // var container = document.getElementById('canvas');
    // var data = {
    //   nodes: nodes,
    //   edges: edges
    // };
    // var options = {
    //   // nodes: {
    //   //   fixed: {
    //   //     x: true,
    //   //     y: true
    //   //   }
    //   // },
    //   edges: {
    //     arrows: 'to'
    //   },
    //   interaction: {
    //     dragNodes: false,
    //     dragView: false,
    //     zoomView: false
    //   },
    //   hierarchical: {
    //     enabled: true,
    //     direction: 'UD'
    //   }
    // };
    // var network = new vis.Network(container, data, options);
    
    // let content = 'st=>start: Start\n\
    //   e=>end: End\n\
    //   op1=>operation: 1\n\
    //   op2=>condition: 2\n\
    //   op3=>operation: 3\n\
    //   op4=>operation: 4\n\
    //   st->op1->op2\n\
    //   op2(yes, right)->op3\n\
    //   op2(no)->op4\n\
    //   op3(right)->op2\n\
    //   op4->e';
      
      
    // let chart = flowchart.parse(content);
    //     chart.drawSVG('canvas', {
    //         // 'x': 30,
    //         // 'y': 50,
    //         'line-width': 3,
    //             'line-length': 25,
    //             'text-margin': 10,
    //             'font-size': 14,
    //             'font': 'normal',
    //             'font-family': 'Helvetica',
    //             'font-weight': 'normal',
    //             'font-color': 'black',
    //             'line-color': 'black',
    //             'element-color': 'black',
    //             'fill': 'white',
    //             'yes-text': 'yes',
    //             'no-text': 'no',
    //             'arrow-end': 'block',
    //             'symbols': {
    //             'start': {
    //                 'font-color': 'red',
    //                     'element-color': 'green',
    //                     'fill': 'yellow'
    //             },
    //                 'end': {
    //                 'class': 'end-element'
    //             }
    //         }
    //     });
  
  }
}
