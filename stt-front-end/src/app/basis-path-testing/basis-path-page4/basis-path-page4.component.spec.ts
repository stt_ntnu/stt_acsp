import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasisPathPage4Component } from './basis-path-page4.component';

describe('BasisPathPage4Component', () => {
  let component: BasisPathPage4Component;
  let fixture: ComponentFixture<BasisPathPage4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasisPathPage4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasisPathPage4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
