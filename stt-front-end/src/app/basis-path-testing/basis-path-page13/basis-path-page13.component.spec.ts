import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasisPathPage13Component } from './basis-path-page13.component';

describe('BasisPathPage13Component', () => {
  let component: BasisPathPage13Component;
  let fixture: ComponentFixture<BasisPathPage13Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasisPathPage13Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasisPathPage13Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
