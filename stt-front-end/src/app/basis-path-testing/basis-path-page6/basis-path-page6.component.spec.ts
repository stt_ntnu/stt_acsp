import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasisPathPage6Component } from './basis-path-page6.component';

describe('BasisPathPage6Component', () => {
  let component: BasisPathPage6Component;
  let fixture: ComponentFixture<BasisPathPage6Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasisPathPage6Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasisPathPage6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
