import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasisPathPage2Component } from './basis-path-page2.component';

describe('BasisPathPage2Component', () => {
  let component: BasisPathPage2Component;
  let fixture: ComponentFixture<BasisPathPage2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasisPathPage2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasisPathPage2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
