import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasisPathTestingComponent } from './basis-path-testing.component';

describe('BasisPathTestingComponent', () => {
  let component: BasisPathTestingComponent;
  let fixture: ComponentFixture<BasisPathTestingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasisPathTestingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasisPathTestingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
