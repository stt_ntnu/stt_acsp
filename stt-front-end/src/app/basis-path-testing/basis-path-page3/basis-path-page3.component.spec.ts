import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasisPathPage3Component } from './basis-path-page3.component';

describe('BasisPathPage3Component', () => {
  let component: BasisPathPage3Component;
  let fixture: ComponentFixture<BasisPathPage3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasisPathPage3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasisPathPage3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
