import { Component, OnInit } from '@angular/core';


//mahesh
// import $ from 'jquery';
// import tooltip from 'bootstrap';
declare var $: any;
// declare var vis:any;

@Component({
  selector: 'app-basis-path-page3',
  templateUrl: './basis-path-page3.component.html',
  styleUrls: ['./basis-path-page3.component.css']
})
export class BasisPathPage3Component implements OnInit {

  constructor() { }

  ngOnInit() {

    $('[data-toggle="tooltip"]').tooltip();

    // create an array with nodes
    // var nodes = new vis.DataSet([
    //   {id: 1, label: 'Start'},
    //   {id: 2, label: 'Node 1'},
    //   {id: 3, label: 'Node 2'},
    //   {id: 4, label: 'Node 3'},
    //   {id: 5, label: 'Node 4'},
    //   {id: 6, label: 'Node 5'},
    //   {id: 7, label: 'End'}
    // ]);

    // create an array with edges
    // var edges = new vis.DataSet([
    //   {from: 1, to: 2},
    //   {from: 2, to: 3},
    //   {from: 3, to: 4},
    //   {from: 3, to: 5},
    //   {from: 5, to: 6},
    //   {from: 4, to: 6},
    //   {from: 6, to: 7}
    // ]);

    // create a network
    // var container = document.getElementById('canvas');
    // var data = {
    //   nodes: nodes,
    //   edges: edges
    // };
    // var options = {
    //   // nodes: {
    //   //   fixed: {
    //   //     x: true,
    //   //     y: true
    //   //   }
    //   // },
    //   edges: {
    //     arrows: 'to'
    //   },
    //   interaction: {
    //     dragNodes: false,
    //     dragView: false,
    //     zoomView: false
    //   }
    // };
    // var network = new vis.Network(container, data, options);

    
    // let content = 'st=>start: Start\n\
    //   e=>end: End\n\
    //   op1=>operation: 1\n\
    //   op2=>condition: 2\n\
    //   op3=>operation: 3\n\
    //   op4=>operation: 4\n\
    //   op5=>operation: 5\n\
    //   st->op1->op2\n\
    //   op2(yes)->op3\n\
    //   op2(no)->op4\n\
    //   op3->op5\n\
    //   op4->op5\n\
    //   op5->e';
      
      
    // let chart = flowchart.parse(content);
    //     chart.drawSVG('canvas', {
    //         // 'x': 30,
    //         // 'y': 50,
    //         'line-width': 3,
    //             'line-length': 25,
    //             'text-margin': 10,
    //             'font-size': 14,
    //             'font': 'normal',
    //             'font-family': 'Helvetica',
    //             'font-weight': 'normal',
    //             'font-color': 'black',
    //             'line-color': 'black',
    //             'element-color': 'black',
    //             'rx':20,
    //             'ry':20,
    //             'fill': 'white',
    //             'yes-text': 'True',
    //             'no-text': 'False',
    //             'arrow-end': 'block',
    //             'symbols': {
    //             'start': {
    //                 // 'font-color': 'red',
    //                 //     'element-color': 'green',
    //                 //     'fill': 'yellow'
    //             },
    //                 'end': {
    //                 'class': 'end-element'
    //             }
    //         }
    //     });
  
  }

  newpath = '';
  buttonDisabled = true;
  errorMessage = false;
  path = '1-2-4-5';

  comparePath = function(){
    var inputpath = this.newpath.replace(/\s/g, "");
    this.errorMessage = true;
    this.buttonDisabled = true;
    if(this.path == inputpath){
      this.errorMessage = false;
      this.buttonDisabled = false;
    }
  }

  // checkEnable = function(){
  //   if(this.buttonDisabled == true){
  //     return false;
  //   }
  //   return true;
  // }
}



