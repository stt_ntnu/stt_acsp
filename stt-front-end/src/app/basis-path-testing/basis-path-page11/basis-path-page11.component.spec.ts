import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasisPathPage11Component } from './basis-path-page11.component';

describe('BasisPathPage11Component', () => {
  let component: BasisPathPage11Component;
  let fixture: ComponentFixture<BasisPathPage11Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasisPathPage11Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasisPathPage11Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
