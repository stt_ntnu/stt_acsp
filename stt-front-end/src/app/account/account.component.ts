import { Component, OnInit } from '@angular/core';
import { AuthService } from './../authentication/auth.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  account: any;

  constructor(public auth: AuthService) { }


  ngOnInit() {
    var serviceAccount = this.auth.account;
    if(serviceAccount){
      this.account = serviceAccount;
    } else{
      this.auth.getAccount((err, profile) => {
        this.account = profile;
      });
    }
  }

}
