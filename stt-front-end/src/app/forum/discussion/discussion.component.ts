import { Component, OnInit, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { IMultiSelectOption, IMultiSelectTexts, IMultiSelectSettings } from 'angular-2-dropdown-multiselect';
import { FormsModule, NgForm }   from '@angular/forms';
import { AuthService}  from '../../authentication/auth.service';

import * as $ from 'jquery';
import 'select2';

import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { AuthHttp } from 'angular2-jwt';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-discussion',
  templateUrl: './discussion.component.html',
  styleUrls: ['./discussion.component.css']
})
export class DiscussionComponent implements OnInit {

  constructor(private auth: AuthService, private authHttp: AuthHttp, private http: HttpClient, private activatedRoute: ActivatedRoute) {
  	auth.handleAuthentication();
  }

  public responseError: boolean = false;
  public responseMessage: string = "";
  public serverError: boolean = false;

  public insertResponseError: boolean = false;
  public insertResponseMessage: string = "";
  public insertServerError: boolean = false;

  public updateResponseError: boolean = false;
  public updateResponseMessage: string = "";
  public updateServerError: boolean = false;

  public api_url: string = environment.api_url + 'forum';

  forum_id;
  forum;
  account;
  discussion_reply;
  edit;

  ngOnInit() {

  	var serviceAccount = this.auth.account;
    if(serviceAccount){
      this.account = serviceAccount;
    } else{
      this.auth.getAccount((err, profile) => {
        this.account = profile;
      });
    }

    this.edit = {discussion: ""};

  	this.activatedRoute.params.subscribe((params: Params) => {
        this.forum_id = params['id'];
      });
  	this.getForum();
  }


  public getForum(): void{
      this.http.get(this.api_url+"/"+this.forum_id)
        .subscribe(res=>{
                this.responseError = false;
                if(res['error']){
                  this.responseError = true;
                  this.responseMessage = res['message'];
                }else{
                  this.forum = res['data'];
                  return;
                }
              },
              err=>{
                this.serverError = true;
              });
    return;
  }

  public newReply(): void{
  	var reply = {
  		discussion: {
  			name: this.account.nickname,
  			email: this.account.name,
  			img: this.account.picture,
  			answer: this.discussion_reply,
  		}
  	};
  	this.authHttp.post(this.api_url+"/"+this.forum_id, reply)
      .map(res => res.json())
        .subscribe(res=>{
                this.insertResponseError = false;
                if(res.error){
                  this.insertResponseError = true;
                  this.insertResponseMessage = res.message;
                }else{
                  this.getForum();
                  this.discussion_reply = "";
                }
              },
              err=>{
                this.insertServerError = true;
              });
    return;
  }


  public removeReply(discussion_id, index): void{
    var thisuser = {email: this.account.name};

    this.authHttp.put(this.api_url+"/"+this.forum_id+"/discussion/"+discussion_id+"/delete", thisuser)
    .map(res=>res.json())
        .subscribe(res=>{
                this.responseError = false;
                if(res.error){
                  this.responseError = true;
                  this.responseMessage = res.message;
                }else{
                  this.forum.discussion.splice(index, 1);
                  return;
                }
              },
              err=>{
                this.serverError = true;
              });
    return;
  }

  public editReply(id, index): void{
  	for (var i = 0; i < this.forum.discussion.length; i++) {
  		if(this.forum.discussion[i]._id == id){
  			this.edit = {
  						discussion: this.forum.discussion[i].answer,
  						id: this.forum.discussion[i]._id
  					};
  		}
  	};
  }


  public updateDiscussion(): void{
  	var updateData = {
  			discussion:{
  				id: this.edit.id,
  				answer: this.edit.discussion,
  				email: this.account.name,
  				img: this.account.picture,
  				name: this.account.nickname,
  			}
  		};

    this.authHttp.put(this.api_url+"/"+this.forum_id, updateData)
    .map(res=>res.json())
        .subscribe(res=>{
                this.updateResponseError = false;
                if(res.error){
                  this.updateResponseError = true;
                  this.updateResponseMessage = res.message;
                }else{
                  this.getForum();
                  $('#questionmodal').find(".close").click();
                  return;
                }
              },
              err=>{
                this.updateServerError = true;
              });
    return;
  }

}
