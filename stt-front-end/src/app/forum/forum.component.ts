import { Component, OnInit, ViewChild } from '@angular/core';
import { IMultiSelectOption, IMultiSelectTexts, IMultiSelectSettings } from 'angular-2-dropdown-multiselect';
import { FormsModule, NgForm }   from '@angular/forms';
import { AuthService}  from '../authentication/auth.service';

import * as $ from 'jquery';
import 'select2';
// import {CommonModule} from '@angular/common';
import { TagInputModule } from 'ngx-chips';

import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { AuthHttp } from 'angular2-jwt';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-forum',
  templateUrl: './forum.component.html',
  styleUrls: ['./forum.component.css']
})
export class ForumComponent implements OnInit {

  constructor(private auth: AuthService, private authHttp: AuthHttp, private http: HttpClient) {
    auth.handleAuthentication();
  }

  public responseError: boolean = false;
  public updateResponseError: boolean = false;
  public responseMessage: string = "";
  public updateResponseMessage: string = "";
  public serverError: boolean = false;
  public updateServerError: boolean = false;
  public api_url: string = environment.api_url + 'forum';
  
  @ViewChild('forumForm') public forumForm: NgForm;

  forumData;
  newQuestion;
  account;

  ngOnInit() {

    var serviceAccount = this.auth.account;
    if(serviceAccount){
      this.account = serviceAccount;
    } else{
      this.auth.getAccount((err, profile) => {
        this.account = profile;
      });
    }

  	$('.select-tags').select2();
  	$('.input-tags').select2({
  		tags: true
  	});

    this.newQuestion = {question: "", description: "", name: "", tags: [], email: "", img:""};
    this.getForum();
  }

  public openNew(): void{
    this.forumForm.reset();
  }

  public getForum(): void{
      this.http.get(this.api_url)
        .subscribe(res=>{
                this.responseError = false;
                if(res['error']){
                  this.responseError = true;
                  this.responseMessage = res['message'];
                }else{
                  this.forumData = res['data'];
                  return;
                }
              },
              err=>{
                this.serverError = true;
              });
    return;
  }

  public addQuestion(): void{
    this.newQuestion.name = this.account.nickname;
    this.newQuestion.email = this.account.name;
    this.newQuestion.img = this.account.picture;
    this.authHttp.post(this.api_url, this.newQuestion)
      .map(res => res.json())
        .subscribe(res=>{
                this.updateResponseError = false;
                if(res.error){
                  this.updateResponseError = true;
                  this.updateResponseMessage = res.message;
                }else{
                  this.getForum();
                  $('#questionmodal').find(".close").click();
                    return;
                }
              },
              err=>{
                this.updateServerError = true;
              });
    return;
  }

  public removeQuestion(forum_id, index): void{
    var thisuser = {email: this.account.name};

    this.authHttp.put(this.api_url+"/"+forum_id+"/delete", thisuser)
    .map(res=>res.json())
        .subscribe(res=>{
                this.responseError = false;
                if(res.error){
                  this.responseError = true;
                  this.responseMessage = res.message;
                }else{
                  this.forumData.splice(index, 1);
                  return;
                }
              },
              err=>{
                this.serverError = true;
              });
    return;
  }

}
