import { Component, OnInit, IterableDiffers, DoCheck, KeyValueDiffers } from '@angular/core';
import { IMultiSelectOption, IMultiSelectTexts, IMultiSelectSettings } from 'angular-2-dropdown-multiselect';
import { FormsModule, NgForm }   from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';

import * as $ from 'jquery';
import 'select2';

import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { AuthHttp } from 'angular2-jwt';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.css']
})
export class QuestionsComponent implements OnInit {

  differ:any;
  objdiffer:any;
  constructor(private authHttp: AuthHttp, private http: HttpClient, private router: Router, private activatedRoute: ActivatedRoute, differs: IterableDiffers, objdiffers: KeyValueDiffers) {
    this.differ = differs.find([]).create(null);
    this.objdiffer = objdiffers.find([]).create(null);
  }

  public answers: any[] = [
    {answer: ''},
  ];
  questionData;
  quiz_id;
  thisQuestion
  public responseError: boolean = false;
  public responseMessage: string = "";
  public serverError: boolean = false;
  public updateResponseError: boolean = false;
  public updateResponseMessage: string = "";
  public updateServerError: boolean = false;
  public answerError: boolean = false;
  public sameAnswerError: boolean = false;
  public sameCorrectAnswerError: boolean = false;
  public api_url:string = environment.api_url + 'questionnaire/quiz';

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
        this.quiz_id = params['id'];
      });
    this.thisQuestion = {
      question: "",
      answer_explanation: "",
      code: "",
      image: "",
      correct_answer: ""
    }
  	$('.select_tags').select2();
    this.getQuestions();
  }

  ngDoCheck() {

    let arrayChange = this.differ.diff(this.answers);
    if(arrayChange){
      this.detechChange();
    }

    for (var i = 0; i < this.answers.length; i++) {
      let change = this.objdiffer.diff(this.answers[i]);
      if(change){
        this.detechChange();
      }      
    }
  }

  public detechChange(): void{
    this.answerError = false;
    this.sameAnswerError = false;
    let count = 0;
    for (var j = 0; j < this.answers.length; j++) {
      if(this.answers[j].answer == this.thisQuestion.correct_answer && this.thisQuestion.correct_answer != ""){
        this.sameAnswerError = true;
      }
      if(this.answers[j].answer == ""){
        count++;
      }
    }
    if(this.sameAnswerError == false){
      this.sameCorrectAnswerError = false;
    }
    if(count == this.answers.length){
      this.answerError = true;
    }
  }

  public checkAnswer():void{
    this.sameCorrectAnswerError = false;
    for (var j = 0; j < this.answers.length; j++) {
      if(this.answers[j].answer == this.thisQuestion.correct_answer && this.thisQuestion.correct_answer != ""){
        this.sameCorrectAnswerError = true;
      }
    }
    if(this.sameCorrectAnswerError == false){
      this.sameAnswerError = false;
    }
  }

  public addAnswer(): void{
    this.answers.push({answer: ''});
  }

  public removeAnswer(i): void{
    this.answers.splice(i, 1);
  }

  public getQuestions(): void{
      this.http.get(this.api_url+'/'+this.quiz_id+'/question')
        .subscribe(res=>{
          console.log(res['data']);
                this.responseError = false;
                if(res['error']){
                  this.responseError = true;
                  this.responseMessage = res['message'];
                }else{
                  this.questionData = res['data'].questions;
                  return;
                }
              },
              err=>{
                this.serverError = true;
              });
    return;
  }

  public removeQuestion(question_id, index): void{
    this.authHttp.delete(this.api_url+"/"+this.quiz_id+'/question/'+question_id)
        .subscribe(res=>{
          console.log(res);
                this.responseError = false;
                if(res['error']){
                  this.responseError = true;
                  this.responseMessage = res['message'];
                }else{
                  this.questionData.splice(index, 1);
                  return;
                }
              },
              err=>{
                this.serverError = true;
              });
    return;
  }

  public getQuestionDetails(question_id): void{
    this.answers = [];
    for (var i = 0; i < this.questionData.length; i++) {
      if(this.questionData[i]._id == question_id){
        this.thisQuestion = {
              _id: this.questionData[i]._id,
              question: this.questionData[i].question,
              code: this.questionData[i].code,
              image: this.questionData[i].img,
              correct_answer: "",
              answer_explanation: this.questionData[i].answer_explanation
            };
        for (var j = 0; j < this.questionData[i].answers.length; j++) {
          if(this.questionData[i].answers[j].correctness == 1){
              this.thisQuestion.correct_answer = this.questionData[i].answers[j].answer;
          }else{
            this.answers.push({answer:this.questionData[i].answers[j].answer});
          }
        };
      }
    }
  }


  public updateQuestion(): void{
    let newAnswer = [];
    let thisCorrectAnswer = {answer: this.thisQuestion.correct_answer, correctness: 1};
    for (var i = 0; i < this.answers.length; i++) {
      if(this.answers[i].answer != ""){
        let thisAnswer = {answer: this.answers[i].answer, correctness: 0};
        newAnswer.push(thisAnswer);
      }
    }
    newAnswer.push(thisCorrectAnswer);

    let updateData = {
      question:{
                id: this.thisQuestion._id,
                question: this.thisQuestion.question,
                code: this.thisQuestion.code,
                img: this.thisQuestion.image,
                answer_explanation: this.thisQuestion.answer_explanation,
                answers: newAnswer
              }
    };

    this.authHttp.put(this.api_url+"/"+this.quiz_id+"/question", updateData)
      .map(res => res.json())
        .subscribe(res=>{
                this.updateResponseError = false;
                if(res.error){
                  this.updateResponseError = true;
                  this.updateResponseMessage = res.message;
                }else{
                  this.getQuestions();
                  $('#questionmodal').find(".close").click();
                    return;
                }
              },
              err=>{
                this.updateServerError = true;
              });
    return;
  }



}
