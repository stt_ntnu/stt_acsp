import { Component, OnInit } from '@angular/core';
import { IMultiSelectOption, IMultiSelectTexts, IMultiSelectSettings } from 'angular-2-dropdown-multiselect';
import { FormsModule, NgForm }   from '@angular/forms';
import * as $ from 'jquery';
import 'select2';

import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { AuthHttp } from 'angular2-jwt';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-admin-quiz',
  templateUrl: './admin-quiz.component.html',
  styleUrls: ['./admin-quiz.component.css']
})
export class AdminQuizComponent implements OnInit {

  constructor(private authHttp: AuthHttp, private http: HttpClient) { }

  myOptions: IMultiSelectOption[];
  	public responseError: boolean = false;
	public responseMessage: string = "";
	public serverError: boolean = false;
  	public api_url:string = environment.api_url + 'questionnaire/quiz';
  	public updateResponseError: boolean = false;
  	public updateServerError: boolean = false;
	public updateResponseMessage: string = "";
	quizData;
	thisQuiz;
	quizSearch;
  mySettings: IMultiSelectSettings;
  myTexts: IMultiSelectTexts;
  
  ngOnInit() {

    this.mySettings = {
          dynamicTitleMaxItems: 0
      };

    this.myTexts = {
      defaultTitle: 'Select'
  };

  	this.myOptions = [
            {id:'data flow testing', name: 'Data Flow Testing' },
            {id:'basis path testing', name: 'Basis Path Testing' }
        ];
  	$('.select_tags, .select-quiz-categories').select2();
  	this.quizSearch = {title:"", category:[]};
  	this.thisQuiz = {title: "", categories: [], description: "", creator: ""};
  	this.getQuiz();
  }

  public getQuiz(): void{
      this.http.get(this.api_url)
        .subscribe(res=>{
        	console.log(res['data']);
                this.responseError = false;
                if(res['error']){
                  this.responseError = true;
                  this.responseMessage = res['message'];
                }else{
                  this.quizData = res['data'];
                  return;
                }
              },
              err=>{
                this.serverError = true;
              });
    return;
  }

  public removeQuiz(quiz_id, index): void{
  	this.authHttp.delete(this.api_url+"/"+quiz_id)
        .subscribe(res=>{
        	console.log(res);
                this.responseError = false;
                if(res['error']){
                  this.responseError = true;
                  this.responseMessage = res['message'];
                }else{
                  this.quizData.splice(index, 1);
                  return;
                }
              },
              err=>{
                this.serverError = true;
              });
    return;
  }

  public getQuizDetails(quiz_id): void{
  	for (var i = 0; i < this.quizData.length; i++) {
  		if(this.quizData[i]._id == quiz_id){
  			this.thisQuiz = {
  						title: this.quizData[i].title,
  						description: this.quizData[i].description,
  						categories: this.quizData[i].categories,
  						creator: this.quizData[i].creator,
  						id: quiz_id
  					};
  		}
  	};
  }

  public updateQuiz(): void{
  	console.log(this.thisQuiz);
  	this.authHttp.put(this.api_url, this.thisQuiz)
  		.map(res => res.json())
        .subscribe(res=>{
                this.updateResponseError = false;
                if(res.error){
                  this.updateResponseError = true;
                  this.updateResponseMessage = res.message;
                }else{
                	this.getQuiz();
                	$('#questionmodal').find(".close").click();
                  	return;
                }
              },
              err=>{
                this.updateServerError = true;
              });
    return;
  }

}
