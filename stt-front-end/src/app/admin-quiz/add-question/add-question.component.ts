import { Component, OnInit, IterableDiffers, DoCheck, KeyValueDiffers } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { AuthHttp } from 'angular2-jwt';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import {NgForm} from '@angular/forms';

import * as $ from 'jquery';
import 'select2';

@Component({
  selector: 'app-add-question',
  templateUrl: './add-question.component.html',
  styleUrls: ['./add-question.component.css']
})
export class AddQuestionComponent implements OnInit {

  differ:any;
  objdiffer:any;
  constructor(private authHttp: AuthHttp, private http: HttpClient, private router: Router, private activatedRoute: ActivatedRoute, differs: IterableDiffers, objdiffers: KeyValueDiffers) {
    this.differ = differs.find([]).create(null);
    this.objdiffer = objdiffers.find([]).create(null);
  }

  questionData;
  quiz_id;
  public formError: boolean = false;
  public answerError: boolean = false;
  public sameCorrectAnswerError: boolean = false;
  public sameAnswerError: boolean = false;
  public serverError: boolean = false;
  public responseError: boolean = false;
  public responseMessage: String = "";
  public errorMessage: string = "Fill the required fields to continue";

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
        this.quiz_id = params['id'];
      });

    this.questionData = {question: "", image: "", code: "", answer_explanation: "", correct_answer: ""};

  }

  ngDoCheck() {

    let arrayChange = this.differ.diff(this.answers);
    if(arrayChange){
      this.detechChange();
    }

    for (var i = 0; i < this.answers.length; i++) {
      let change = this.objdiffer.diff(this.answers[i]);
      if(change){
        this.detechChange();
      }      
    }
  }

  public detechChange(): void{
    this.answerError = false;
    this.sameAnswerError = false;
    let count = 0;
    for (var j = 0; j < this.answers.length; j++) {
      if(this.answers[j].answer == this.questionData.correct_answer && this.questionData.correct_answer != ""){
        this.sameAnswerError = true;
      }
      if(this.answers[j].answer == ""){
        count++;
      }
    }
    if(this.sameAnswerError == false){
      this.sameCorrectAnswerError = false;
    }
    if(count == this.answers.length){
      this.answerError = true;
    }
  }

  public checkAnswer():void{
    this.sameCorrectAnswerError = false;
    for (var j = 0; j < this.answers.length; j++) {
      if(this.answers[j].answer == this.questionData.correct_answer && this.questionData.correct_answer != ""){
        this.sameCorrectAnswerError = true;
      }
    }
    if(this.sameCorrectAnswerError == false){
      this.sameAnswerError = false;
    }
  }

  public answers: any[] = [
    {answer: ''},
  ];

  public addAnswer(): void{
    this.answers.push({value: ''});
  }

  public removeAnswer(i): void{
    this.answers.splice(i, 1);
  }


  public save(questionForm: NgForm):void{
    this.submitData("save", questionForm);
  }

  public saveAndAdd(questionForm: NgForm): void{
    this.submitData("saveAndAdd", questionForm);
  }

  public submitData(saveType, questionForm: NgForm):void{

    let count = 0;
    for(let i=0; i< this.answers.length; i++){
      this.answers[i].correctness = 0;
    }

    let newAnswers = [];
    let correct_answer = {answer: this.questionData.correct_answer, correctness: 1};
    for (var i = 0; i < this.answers.length; i++) {
      newAnswers.push(this.answers[i]);
    };
    newAnswers.push(correct_answer);

    let newQuestion = {
      "questions": {
              "answer_explanation": this.questionData.answer_explanation,
              "question": this.questionData.question,
              "code": this.questionData.code,
              "image": this.questionData.image,
              "answers": newAnswers
      }
    }

    var api_url = environment.api_url + 'questionnaire/quiz/'+this.quiz_id+'/question';

      this.authHttp.post(api_url, newQuestion)
        .map(res => res.json())
        .subscribe(res=>{
                this.responseError = false;
                if(res.error){
                  this.responseError = true;
                  this.responseMessage = res.message;
                }else{
                  var routeLink = '/admin-quiz/'+this.quiz_id+'/questions';
                  if(saveType == "saveAndAdd"){
                    questionForm.reset();
                  }else{
                    this.router.navigateByUrl(routeLink);
                  }

                  return;
                }
              },
              err=>{
                this.serverError = true;
              });
    return;

  }

}
