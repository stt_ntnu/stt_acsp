import { Component, OnInit, IterableDiffers, DoCheck, KeyValueDiffers } from '@angular/core';
import { Router } from '@angular/router';

import { FormsModule, NgForm }   from '@angular/forms';
import { IMultiSelectOption, IMultiSelectTexts, IMultiSelectSettings } from 'angular-2-dropdown-multiselect';

import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { AuthService } from '../../authentication/auth.service';

import { AuthHttp } from 'angular2-jwt';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import * as $ from 'jquery';
import 'select2';


@Component({
  selector: 'app-add-quiz',
  templateUrl: './add-quiz.component.html',
  styleUrls: ['./add-quiz.component.css']
})
export class AddQuizComponent implements OnInit {

  objdiffer:any;
  differ:any;
  account: any;

  constructor(private authHttp: AuthHttp, private http: HttpClient, private router: Router, differs: IterableDiffers, objdiffers: KeyValueDiffers, public auth: AuthService) {
    this.differ = differs.find([]).create(null);
    this.objdiffer = objdiffers.find([]).create(null);
  }

  public answers: any[] = [
    {answer: ''},
  ];
  quizData;
  questionData;
  myOptions: IMultiSelectOption[];


  public first: boolean = true;
  public firstError: boolean = false;
  public secondError: boolean = false;
  public answerError: boolean = false;
  public sameAnswerError: boolean = false;
  public sameCorrectAnswerError: boolean = false;
  public serverError: boolean = false;
  public responseError: boolean = false;
  public responseMessage: String = "";
  public errorMessage: string = "Fill the required fields to continue";

  ngOnInit() {

    var serviceAccount = this.auth.account;
    if(serviceAccount){
      this.account = serviceAccount;
    } else{
      this.auth.getAccount((err, profile) => {
        this.account = profile;
      });
    }

    this.quizData = {title:"", category:[], description:""};
    this.questionData = {question: "", image: "", code: "", answer_explanation: "", correct_answer: ""};

    this.myOptions = [
            {id:'data flow testing', name: 'Data Flow Testing' },
            {id:'basis path testing', name: 'Basis Path Testing' },
        ];

    $('.select_categories').select2();
  }


  ngDoCheck() {

    let arrayChange = this.differ.diff(this.answers);
    if(arrayChange){
      this.detechChange();
    }

    for (var i = 0; i < this.answers.length; i++) {
      let change = this.objdiffer.diff(this.answers[i]);
      if(change){
        this.detechChange();
      }      
    }
  }

  public detechChange(): void{
    this.answerError = false;
    this.sameAnswerError = false;
    let count = 0;
    for (var j = 0; j < this.answers.length; j++) {
      if(this.answers[j].answer == this.questionData.correct_answer && this.questionData.correct_answer != ""){
        this.sameAnswerError = true;
      }
      if(this.answers[j].answer == ""){
        count++;
      }
    }
    if(this.sameAnswerError == false){
      this.sameCorrectAnswerError = false;
    }
    if(count == this.answers.length){
      this.answerError = true;
    }
  }

  public checkAnswer():void{
    this.sameCorrectAnswerError = false;
    for (var j = 0; j < this.answers.length; j++) {
      if(this.answers[j].answer == this.questionData.correct_answer && this.questionData.correct_answer != ""){
        this.sameCorrectAnswerError = true;
      }
    }
    if(this.sameCorrectAnswerError == false){
      this.sameAnswerError = false;
    }
  }

  public addAnswer(): void{
    this.answers.push({answer: ''});
  }

  public removeAnswer(i): void{
    this.answers.splice(i, 1);
  }

  public nextPage(): void{
    this.first = false;
  }

  public prevPage(): void{
    this.first = true;
  }

  public save(isValid: boolean):void{
    this.secondError = true;
    if(isValid){
      this.secondError = false;
      this.submitData();
    }
  }

  public submitData():void{

    let count = 0;
    for(let i=0; i< this.answers.length; i++){
      if(this.answers[i].answer == ""){
        count++;
      }
      if(this.questionData.correct_answer == this.answers[i].answer){
        this.sameAnswerError = true;
        return;
      }
      this.sameAnswerError = false;
      this.answers[i].correctness = 0;
    }

    if(count == this.answers.length){
      this.answerError = true;
      return;
    }
    this.answerError = false;


    let newAnswers = [];
    let correct_answer = {answer: this.questionData.correct_answer, correctness: 1};
    for (var i = 0; i < this.answers.length; i++) {
      if(this.answers[i].answer != ""){
        newAnswers.push(this.answers[i]);
      }
    };
    newAnswers.push(correct_answer);

    let newQuiz = {
      "creator": this.account.nickname,
      "title": this.quizData.title,
      "description": this.quizData.description,
      "categories": this.quizData.category,
      "questions": [{
              "answer_explanation": this.questionData.answer_explanation,
              "question": this.questionData.question,
              "answers": newAnswers
      }]
    }

    var api_url = environment.api_url + 'questionnaire/quiz';
      this.authHttp.post(api_url, newQuiz)
        .map(res => res.json())
        .subscribe(res=>{
                this.responseError = false;
                if(res.error){
                  this.responseError = true;
                  this.responseMessage = res.message;
                }else{
                  this.router.navigateByUrl('/admin-quiz/'+res.data.quiz_id+'/questions');
                  return;
                }
              },
              err=>{
                this.serverError = true;
              });
    return;

  }
}
