import { Component, OnInit } from '@angular/core';

import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { AuthHttp } from 'angular2-jwt';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})
export class QuizComponent implements OnInit {

  quiz:Quiz;
  quizzes: Quiz[] = [];
  selectedCategories: string[] = ['data flow testing', 'basis path testing'];
  question1:Question;
  question2:Question;
  question3:Question;
  question4:Question;
  question5:Question;
  activeQuestion:number = 0;
  selectedAnswer:number = 4;
  selectedQuiz = -1;
  results:number[] = [];
  quizSelected:boolean = false;
  quizComplete:boolean = false;
  alert:boolean = false;
  
  
  constructor(private http: HttpClient) { }

  ngOnInit() {

    this.quiz = {
      creator:'Vegar Lerpold',
      title:'Basic DFT',
      description:'Questions about the basics of Data Flow Testing',
      categories:['data flow testing'],
      questions:[]
    }
    this.question1 = {
      question:'What is the main focus of Data Flow Testing?',
      answers:[
        {answer:'To make sure that all data values are defined and used correctly', correctness:1},
        {answer:'To enable all the data in a program to flow both ways', correctness:0},
        {answer:'To ensure that none of the variables have the same value', correctness:0}
      ],
      answer_explanation: 'In Data Flow Testing, we need to identify paths in the program that go from the assignment of a value to a variable, to the use of such variable. This is done to make sure that the variable is properly used. Specifically, we should try to ensure that the values created at one point in the program are defined and used correctly.'
    }
    this.question2 = {
      question: 'What can you NOT identify with Data Flow Testing?',
      answers: [
        {answer:'A variable that is declared but never used within the program', correctness:0},
        {answer:'The deallocation of a variable before it is used', correctness:0},
        {answer:'A variable that contains a valid, but unintended, vaule', correctness:1}
      ],
      answer_explanation: 'When a variable has been assigned a valid value, we can not use Data Flow Testing to check the variables content.'
    }
    this.question3 = {
      question: 'Is "x=42+y" a definition or a use of the variable x?',
      answers: [
        {answer:'Definition', correctness:1},
        {answer:'Use', correctness:0},
        {answer:'Both a definition and a use', correctness:0},
        {answer:'HA!', correctness:0}
      ],
      answer_explanation: 'x is defined to be the sum of 42 and y.'
    }
    this.question4 = {
      question: 'Is "if(x==1)" a definition or a use of the variable x?',
      answers: [
        {answer:'Definition', correctness:0},
        {answer:'Use', correctness:1},
        {answer:'Neither a definition or a use', correctness:0}
      ],
      answer_explanation: 'x is used in an if-construct, as part of a boolean condition.'
    }
    this.question5 = {
      question: 'Is "x=42+x" a definition or a use of the variable x?',
      answers: [
        {answer:'Definition', correctness:0},
        {answer:'Use', correctness:0},
        {answer:'Both a definition and a use', correctness:1}
      ],
      answer_explanation: 'Here, x is used to define its new value. It is therefore both a definition and a use.'
    }
    this.quiz.questions.push(this.question1);
    this.quiz.questions.push(this.question2);
    this.quiz.questions.push(this.question3);
    this.quiz.questions.push(this.question4);
    this.quiz.questions.push(this.question5);
    this.quizzes.push(this.quiz);
    this.quizzes.push(this.quiz);
    this.quizzes.push(this.quiz);
    this.quizzes.push(this.quiz);
    this.quizzes.push(this.quiz);

    this.getQuizData();
  }

  public getQuizData(): void{
    var url = environment.api_url+"questionnaire/quiz";
    this.http.get(url)
    .subscribe(
        response =>{
          this.quizzes = response['data'];
          console.log(response['error']);
          console.log(response['message']);
        },
        error => {
          console.log(error);
        }
      );
  }

  public shuffleAnswers(q:Question) {
    var j, x, i;
    for (i = q.answers.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = q.answers[i];
        q.answers[i] = q.answers[j];
        q.answers[j] = x;
    }
}

  getQuestion(at:number){
    return this.quiz.questions[at].question;
  }

  questionAnswered(){
    if(this.selectedAnswer==4){
      this.alert=true;
      return;
    }

    this.results.push(this.selectedAnswer);
    this.alert=false;
    
    if(this.activeQuestion<(this.quiz.questions.length)-1){
      this.activeQuestion++;
      this.selectedAnswer = 4;
    }
    else{
      this.quizComplete=true;
    }
  }

  selectAnswer(n:number){
    this.selectedAnswer = n;
  }

  getScore(){
    this.activeQuestion=0;
    var score:number = 0;
    
    for(let i of this.results){

      score=score+(this.quiz.questions[this.activeQuestion].answers[i].correctness);
      this.activeQuestion++;
    }

    return score;
  }
  getPercentage(){
    
    var score:number=this.getScore();
    var total:number=this.quiz.questions.length;
    return score/total*100;
  }

  getCorrectAnswer(q:Question){
    for(let a of q.answers){
      if(a.correctness==1){
        return a;
      }
    }
  }
  startQuiz(){
    for(let q of this.quiz.questions){
      this.shuffleAnswers(q);
    }
    this.quizSelected = true;
  }
  resetQuiz(){
    this.activeQuestion = 0;
    this.selectedAnswer = 4;
    this.selectedQuiz = -1;
    this.results = [];
    this.quizSelected = false;
    this.quizComplete = false;

  }

  dataFlowChange(e){
    if(e.target.checked){
      this.selectedCategories.push('data flow testing');      
    }
    else{
      var index = this.selectedCategories.indexOf('data flow testing', 0);
      if (index > -1) {
         this.selectedCategories.splice(index, 1);
      }
    }
  }

  basisPathChange(e){
    if(e.target.checked){
      this.selectedCategories.push('basis path testing');      
    }
    else{
      var index = this.selectedCategories.indexOf('basis path testing', 0);
      if (index > -1) {
         this.selectedCategories.splice(index, 1);
      }
    }
  }

  filter(q:Quiz){
    
    var found = false;

    for(let tag of q.categories){
      for(let selectTag of this.selectedCategories){
        if(tag==selectTag){
          found = true;
        }
      }
      if(found==false){
        return false;
      }
      found=false;
    }
    return true;
  }
}

export interface Quiz{
  creator:string;
  title:string;
  description:string;
  categories:string[];
  questions:Question[];
}

export interface Question{
  question:string;
  answers:Answer[];
  answer_explanation:string;
}

export interface Answer{
  answer:string;
  correctness:number;
}