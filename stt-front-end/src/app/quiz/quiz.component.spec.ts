import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizComponent } from './quiz.component';

describe('QuizComponent', () => {
  let component: QuizComponent;
  let fixture: ComponentFixture<QuizComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });


  it('should have its questions defined with valid values for question, answers and answer_explanation properties', () => {
    component.quiz.questions.forEach(element => {
      expect(element.question.length >= 1);
      expect(element.answers.length >= 2);
      var hasCorrectAnswer = false;
      element.answers.forEach(innerElement => {
        expect(innerElement.answer.length >= 1)
        expect(innerElement.correctness == 1 || innerElement.correctness == 0)
        if (innerElement.correctness == 1){
          hasCorrectAnswer = true;
        }
      });
      expect(hasCorrectAnswer);
      expect(element.answer_explanation.length >= 1);
    }); 
  });

/*   it('should be possible to receive full score on quiz', () => {
    var correctAnswers = [0,2,0,1,2]
    while(!component.quizComplete){
      component.selectAnswer(correctAnswers[component.activeQuestion])
      component.questionAnswered();
    }
    expect(component.score == component.quiz.questions.length);
  }); */

  it('should be defined a creator, title, description and categories quiz belong to', () => {
    expect(component.quiz.creator.length >= 1)
    expect(component.quiz.title.length >= 1)
    expect(component.quiz.description.length >= 1)
    expect(component.quiz.categories.length >= 1)
  });

});
