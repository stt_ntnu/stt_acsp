const express = require('express');
const path = require('path');
const config = require('./server/config');
var bodyParser = require('body-parser');

//These routes contains test calls for making secure calls to the server
var routes = require('./routes/index');

//These routes contains calls related to the DB
var questionnaire = require('./routes/questionnaire');

var forum = require('./routes/forum');

var pug = require('pug');

var db = require('./server/db/connection');
var dbQueries =  require('./server/db/testdata');
//dbQueries.addTestData();

const app = express();

// This isn't really used but it might be used in the future for debugging purposes
// with the server/DB connection - pug renders HTML (on the server-side in this case).
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug'); 

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes); 
app.use('/questionnaire', questionnaire);

app.use('/forum', forum);

app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
}); 

app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.send(err);
});

var port = config.PORT;
app.set('port', port);
app.listen(port, () => console.log(`Running @ localhost:${port}`));