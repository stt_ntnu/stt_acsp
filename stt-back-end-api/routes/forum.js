var express = require('express');

var Forum = require('../server/db/models/forum');
var Discussion = require('../server/db/models/discussion');

var router = express.Router();
var auth = require('../server/authentication');
var dbConnection = require('../server/db/connection');

/**
* Get the list of quiz with details
**/
router.get('/', function (req, res) {
	
	Forum.find(function (error, response) {

		if (error) {
			res.json({
					message: "Sorry! Unable to get forum data. Please check the request and retry.",
					error: true
				});
			return;
		}

		res.json({
					message: "Forum Data.",
					data: response,
					error: false
				});
	});
});

/**
* Add new forum question
**/
router.post('/', auth.checkJwt, auth.guard.check('user'), function (req, res, next) {
		var data = req.body;


		var question1 = new Forum(data);
        question1.validate(function(error){
        	if(error){
	        	res.json({
					message: "Not all the required fields are provided. Please check and retry.",
					error: true
				});
			}else{
		        question1.save(function (err, response) {
		            if (err) {
		                res.json({
							message: "Sorry! Unable to save the question. Please check the request data and retry.",
							data: {},
							error: true
						});
		            } else {
		                res.json({
							message: "Forum Question saved Successfully.",
							data: {
								forum_id: response._id
							},
							error: false
						});
		            }
		        });
			}
        });
});


/**
* delete the forum question
**/
router.put('/:id/delete', auth.checkJwt, auth.guard.check('user'), function (req, res) {

		Forum.findById(req.params.id, function(error, forum){
			if(error){
				res.json({
						message: "Sorry! There seems to be an error. Please check your request.",
						error: true
					});
				return;
			}
			if(!forum){
				res.json({
						message: "Sorry! Forum not found. Please check your request.",
						error: true
					});
				return;	
			}

			if(forum.email != req.body.email){
				res.json({
						message: "Sorry! You are not authorized to perform this action.",
						error: true
					});
				return;	
			}


			Forum.remove({_id: req.params.id}, function (err, response) {
				if (err) {
					res.json({
						message: "Sorry! Unable to remove the forum question. Please check your request data and retry.",
						error: true
					});
					return;
				}
				res.json({
					message: "Forum Question has been removed Successfully!",
					data: response,
					error: false
				});
			});
		});
	
});

/**
* Get the discussion and questions associated with a forum
* @param id -> quiz_id
**/
router.get('/:id', function (req, res) {
	
		Forum.findById(req.params.id, function (error, response) {
			if (error) {
				res.json({
					message: "Sorry! There seems to be an error. Please check your request.",
					error: true
				});
				return;
			}
			if(!response){
				res.json({
						message: "Sorry! Forum not found. Please check your request.",
						error: true
					});
				return;
			}
			res.json({
					message: "Forum Discussion!",
					data: response,
					error: false
				});
		});
	
});


/**
* Add new discussion to the forum
**/
router.post('/:id', auth.checkJwt, auth.guard.check('user'), function (req, res) {
	
		Forum.findById(req.params.id, function(error, forum){
			if(error){
				res.json({
						message: "Sorry! There seems to be an error. Please check your request.",
						error: true
					});
				return;
			}
			if(!forum){
				res.json({
						message: "Sorry! Forum Question not found. Please check your request.",
						error: true
					});
				return;
			}

			thisDiscussion = new Discussion(req.body.discussion);
			var checkError = false;
			thisDiscussion.validate(function(error){
		    	if(error){
					checkError = true;
				}
		    });

		    if(checkError){
		    	res.json({
					message: "Not all the required fields are provided. Please check and retry.",
					error: true
				});
				return;
		    }

			Forum.update({_id: req.params.id}, {$push: {"discussion": req.body.discussion}}, {new: true}, function (err, response) {
				if (err) {
					res.json({
						message: "Sorry! There seems to be an error. Please check your request.",
						error: true
					});
					return;
				}

				res.json({
					message: "Discussion has been saved Successfully!",
					data: {forum_id: req.params.id},
					error: false
				});
			});
		});
	
});

/**
* Update discussion to the forum
**/
router.put('/:id', auth.checkJwt, auth.guard.check('user'), function (req, res) {
	
		Forum.findOne({ _id: req.params.id, "discussion._id": req.body.discussion.id }, 'discussion.$', function (error, response) {
			if(error){
				res.json({
						message: "Sorry! There seems to be an error. Please check your request.",
						error: true
					});
				return;
			}

			if(!response){
				res.json({
						message: "Sorry! Discussion for the forum question not found. Please check your request.",
						error: true
					});
				return;
			}else{
				
				if(response.discussion[0].email != req.body.discussion.email){
					res.json({
						message: "Sorry! You are not authorized to perform this action.",
						error: true
					});
					return;	
				}

				thisDiscussion = new Discussion(req.body.discussion);
				var checkError = false;
				thisDiscussion.validate(function(error){
					if(error){
						checkError = true;
					}
				});
				if(checkError){
					res.json({
						message: "Not all the required fields are provided. Please check and retry.",
						error: true
					});
					return;
				}


				Forum.update({_id: req.params.id, "discussion._id": req.body.discussion.id}, {$set: {"discussion.$": req.body.discussion}}, function (err, response) {
					if (err) {
						res.json({
							message: "Sorry! There seems to be an error. Please check your request.",
							error: true
						});
						return;
					}
					res.json({
						message: "Discussion has been updated Successfully!",
						data: {forum_id: req.params.id, discussion_id: req.body.discussion.id},
						error: false
					});
				});
			}
		});
});


/**
* delete discussion to the forum
**/
router.put('/:id/discussion/:did/delete', auth.checkJwt, auth.guard.check('user'), function (req, res) {
		Forum.findOne({ _id: req.params.id, "discussion._id": req.params.did }, 'discussion.$', function (error, response) {
			if(error){
				res.json({
						message: "Sorry! There seems to be an error. Please check your request.",
						error: true
					});
				return;
			}

			if(!response){
				res.json({
						message: "Sorry! Discussion for the forum question not found. Please check your request.",
						error: true
					});
				return;
			}else{

				if(response.discussion[0].email != req.body.email){
					res.json({
						message: "Sorry! You are not authorized to perform this action.",
						error: true
					});
					return;	
				}

				Forum.update({_id: req.params.id, "discussion._id": req.params.did}, {$pull: {"discussion": {_id: req.params.did}}}, function (err, response) {
					if (err) {
						res.json({
							message: "Sorry! There seems to be an error. Please check your request.",
							error: true
						});
					}else{
						res.json({
							message: "Discussion has been removed Successfully!",
							data: {forum_id: req.params.id},
							error: false
						});
					}
				});
			}
		});
	
});



module.exports = router;