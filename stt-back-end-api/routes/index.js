var express = require('express');
var router = express.Router();
var auth = require('../server/authentication')

/**
 * Configuers CORS to allow requests through to the server
 */
router.use(function (req, res, next) {
    console.log("Something is happening.");
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.header("Access-Control-Allow-Methods", "GET, POST, DELETE, UPDATE, OPTIONS, PUT");
    if (req.method === "OPTIONS") {
        return res.status(200).end();
    }
    next();
});

/**
 * A authenticated call requireing: signed-in-user
 */
router.get('/api/private', auth.checkJwt, function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send({ message: "You successfully accessed a secured endpoint!" });
});

/**
 * A authenticated call requirering: signed-in-user with admin-role
 * A user with admin-role will also have every role with less privliges, i.e. admin will always also be teacher, student and user 
 */
router.get('/api/private/admin', auth.checkJwt, auth.guard.check('admin'), function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send({ message: "You successfully accessed a ADMIN endpoint!" });
});

/**
 * A authenticated call requirering: signed-in-user with teacher-role
 * A user with teacher-role will also have every role with less privliges, i.e. teacher will always also be student and user
 */
router.get('/api/private/teacher', auth.checkJwt, auth.guard.check('teacher') ,function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send({ message: "You successfully accessed a TEACHER endpoint!" });
});

/**
 * A authenticated call requirering: signed-in-user with student-role
 * A user with student-role will also have every role with less privliges, i.e. student will always also be a user
 */
router.get('/api/private/student', auth.checkJwt, auth.guard.check('student') ,function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send({ message: "You successfully accessed a STUDENT endpoint!" });
});

/**
 * A authenticated call requirering: signed-in-user with user-role
 * A user with user-role will not have any other role since this is the lowest in the hiearchy
 * A normal authenticated call may just as well be used
 */
router.get('/api/private/user', auth.checkJwt, auth.guard.check('user') ,function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send({ message: "You successfully accessed a USER endpoint!" });
});

/**
 * A unauthenticated call accessible by anybody, i.e. the visitor doesn't need to be a registered user to access this resource.
 */
router.get('/api/public', function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send({ message: "You accessed a public endpoint!" });
});

module.exports = router;