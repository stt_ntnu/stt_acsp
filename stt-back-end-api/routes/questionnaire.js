var express = require('express');

var Question = require('../server/db/models/question');
var Quiz = require('../server/db/models/quiz');

var router = express.Router();
var auth = require('../server/authentication')
var dbConnection = require('../server/db/connection');

/**
* Get the list of quiz with details
**/
router.get('/quiz', function (req, res) {
	
		Quiz.find(function (error, response) {
			if (error) {
				res.json({
						message: "Sorry! Unable to get quiz data. Please check the request and retry.",
						error: true
					});
				return;
			}

			// for (var key in response) {
			// 	response[key].questions = undefined;
			// }

			res.json({
						message: "Quiz Data.",
						data: response,
						error: false
					});
		});
	
});


/**
* Add new quiz but atleast a question is required
**/
router.post('/quiz', auth.checkJwt, auth.guard.check('teacher'), function (req, res) {
		var data = req.body;

		var allQuestions = [];
		for (var key in data.questions) {
			var eachQuestion = new Question(data.questions[key]);
			allQuestions[key] = eachQuestion;
		}

		var quiz1 = new Quiz(
			{
				creator: data.creator,
				title: data.title,
				description: data.description,
				categories: data.categories,
				questions: allQuestions
			}
		);

		var checkError = false;
		quiz1.validate(function(error){
			if(error){
				res.json({
					message: "Not all the required fields are provided. Please check and retry.",
					error: true
				});
				return;
			}else{
				quiz1.save(function (err, response) {
					if (err) {
						res.json({
							message: "Sorry! Unable to save quiz data. Please check the request data and retry.",
							error: true
						});
						return;
					} else {
						res.json({
							message: "Quiz save Successfully!.",
							data: {quiz_id: response._id},
							error: false
						});
					}
				});
			}
		});
	
});

/**
* Update the quiz
**/
router.put('/quiz', auth.checkJwt, auth.guard.check('teacher'), function (req, res) {
	
		Quiz.findById(req.body.id, function(error, quiz){
			if(error){
				res.json({
						message: "Sorry! There seems to be an error. Please check your request.",
						error: true
					});
				return;
			}
			if(!quiz){
				res.json({
						message: "Sorry! Quiz not found. Please check your request.",
						error: true
					});
				return;
			}

			for (var key in req.body) {
				if(req.body[key] == ""){
					res.json({
						message: "Not all the required fields are provided. Please check and retry.",
						error: true
					});
					return;
				}
			}

			Quiz.update({_id: req.body.id}, {$set: req.body}, function (err, response) {
				if (err) {
					res.json({
						message: "Sorry! There seems to be an error. Please check your request.",
						error: true
					});
					return;
				}
				res.json({
						message: "Quiz updated Successfully!.",
						data: {quiz_id: req.body.id},
						error: false
					});
			});
		});
	
});


/**
* delete the quiz
**/
router.delete('/quiz/:id', auth.checkJwt, auth.guard.check('teacher') , function (req, res) {
	
		Quiz.findById(req.params.id, function(error, quiz){
			if(error){
				res.json({
						message: "Sorry! There seems to be an error. Please check your request.",
						error: true
					});
				return;
			}
			if(!quiz){
				res.json({
						message: "Sorry! Quiz not found. Please check your request.",
						error: true
					});
				return;	
			}
			Quiz.remove({_id: req.params.id}, function (err, response) {
				if (err) {
					res.json({
						message: "Sorry! Unable to remove the quiz. Please check your request data and retry.",
						error: true
					});
					return;
				}
				res.json({
					message: "Quiz has been removed Successfully!",
					data: response,
					error: false
				});
			});
		});
	
});

/**
 * Gets quizzes matching a creator
 * 
 * 
 * 
 * 
 */
router.get('/quiz/creator/:creatorName', function (req, res) {
	
		Quiz.find({ creator: {$regex: req.params.creatorName, $options: "i"} }, function (err, response) {
			if (err) {
				res.json({
					message: "Sorry! There seems to be an error. Please check your request.",
					error: true
				});
				return;
			}
			res.json({
					message: "Quiz data!",
					data: response,
					error: false
				});
		});
	
});

/**
 * Gets quizzes matching a category
 */
router.get('/quiz/category/:categoryName', function (req, res) {
	
		Quiz.find({ categories: {$regex: req.params.categoryName, $options: "i"} }, function (err, response) {
			if (err) {
				res.json({
					message: "Sorry! There seems to be an error. Please check your request.",
					error: true
				});
				return;
			}
			res.json({
					message: "Quiz data!",
					data: response,
					error: false
				});
		});
	
});

/**
* Get the questions associated with a quiz with details
* @param id -> quiz_id
**/
router.get('/quiz/:id/question', function (req, res) {
	
		Quiz.findById(req.params.id, function (error, response) {
			if (error) {
				res.json({
					message: "Sorry! There seems to be an error. Please check your request.",
					error: true
				});
				return;
			}
			if(!response){
				res.json({
						message: "Sorry! Quiz not found. Please check your request.",
						error: true
					});
				return;
			}
			res.json({
					message: "Quiz Questions!",
					data: response,
					error: false
				});
		});
	
});


/**
* Add new question to the quiz
**/
router.post('/quiz/:id/question', function (req, res) {
	
		Quiz.findById(req.params.id, function(error, quiz){
			if(error){
				res.json({
						message: "Sorry! There seems to be an error. Please check your request.",
						error: true
					});
				return;
			}
			if(!quiz){
				res.json({
						message: "Sorry! Quiz not found to add question. Please check your request.",
						error: true
					});
				return;
			}

			thisQuestion = new Question(req.body.questions);
			var checkError = false;
			thisQuestion.validate(function(error){
				if(error){
					res.json({
						message: "Not all the required fields are provided. Please check and retry.",
						error: true
					});
					return;
				}else{
					Quiz.update({_id: req.params.id}, {$push: {"questions": new Question(req.body.questions)}}, {new: true}, function (err, response) {
							  // res.json(err);return;
						if (err) {
							res.json({
								message: "Sorry! There seems to be an error. Please check your request.",
								error: true
							});
							return;
						}
						// res.json(response);return;
						res.json({
							message: "Question has been saved Successfully!",
							data: {quiz_id: req.params.id},
							error: false
						});
					});
				}
			});
		});
	
});

/**
* Update question to the quiz
**/
router.put('/quiz/:id/question', auth.checkJwt, auth.guard.check('teacher'), function (req, res) {
	
		Quiz.findOne({ _id: req.params.id, "questions._id": req.body.question.id }, 'questions.$', function (error, response) {
			if(error){
				res.json({
						message: "Sorry! There seems to be an error. Please check your request.",
						error: true
					});
				return;
			}

			if(!response){
				res.json({
						message: "Sorry! Question for the quiz not found. Please check your request.",
						error: true
					});
				return;
			}else{

				thisQuestion = new Question(req.body.question);
				var checkError = false;
				thisQuestion.validate(function(error){
					if(error){
						res.json({
							message: "Not all the required fields are provided. Please check and retry.",
							error: true
						});
						return;
					}else{
						Quiz.update({_id: req.params.id, "questions._id": req.body.question.id}, {$set: {"questions.$": req.body.question}}, function (err, response) {
							if (err) {
								res.json({
									message: "Sorry! There seems to be an error. Please check your request.",
									error: true
								});
								return;
							}
							res.json({
								message: "Question has been updated Successfully!",
								data: {quiz_id: req.params.id, question_id: req.body.question.id},
								error: false
							});
						});
					}
				});
			}
		});
	
});


/**
* delete question to the quiz
**/
router.delete('/quiz/:id/question/:qid', auth.checkJwt, auth.guard.check('teacher') , function (req, res) {
	
		Quiz.findOne({ _id: req.params.id, "questions._id": req.params.qid }, 'questions.$', function (error, response) {
			if(error){
				res.json({
						message: "Sorry! There seems to be an error. Please check your request.",
						error: true
					});
				return;
			}

			if(!response){
				res.json({
						message: "Sorry! Question for the quiz not found. Please check your request.",
						error: true
					});
				return;
			}else{

				Quiz.update({_id: req.params.id, "questions._id": req.params.qid}, {$pull: {"questions": {_id: req.params.qid}}}, function (err, response) {
					if (err) {
						res.json({
							message: "Sorry! There seems to be an error. Please check your request.",
							error: true
						});
						return;
					}
					res.json({
						message: "Quiz has been removed Successfully!",
						data: {quiz_id: req.params.id},
						error: false
					});
				});
			}
		});
	
});


/**
* Get the questions details of a quiz
* @param id, qid
* id->quiz_id, qid->question_id
**/
router.get('/quiz/:id/question/:qid', function (req, res) {
	
		Quiz.findOne({ _id: req.params.id, "questions._id": req.params.qid }, 'questions.$', function (error, response) {
			if (error) {
				res.json({
					message: "Sorry! There seems to be an error. Please check your request.",
					error: true
				});
				return;
			}

			if(!response){
				res.json({
						message: "Sorry! Question for the quiz not found. Please check your request.",
						error: true
					});
				return;
			}
			
			res.json({
					message: "Quiz Question!",
					data: response,
					error: false
				});
		});
	
});

module.exports = router;