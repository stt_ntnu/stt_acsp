
let mongoose = require('mongoose');
let server = require('../app');
var expect  = require('chai').expect;
var request = require('request');
let chaiHttp = require('chai-http');
var Question = require('../server/db/models/question');
var Quiz = require('../server/db/models/quiz');
let chai = require('chai');
let should = chai.should();

chai.use(chaiHttp);

var sendData = {
    "creator": "Mr. Smith",
    "title": "Random questions",
    "description": "A couple of questions testing your knowlege on mathematics",
    "questions": [
        {
            "answer_explanation": "Perform simple multiplication...",
            "question": "What is 12x21?",
            "answers": [
                {
                    "answer": "1221",
                    "correctness": 0
                },
                {
                    "answer": "252",
                    "correctness": 1
                },
                {
                    "answer": "135",
                    "correctness": 0
                }
            ]
        }
    ],
    "categories": [
        "data flow testing",
        "white-box testing"
    ]
};

describe ('Main page', function() {
        it('main page status', function(done){
            	      // assert.equal(1, -1);
            request('http://localhost:3000', function(error, response, body) {
            	// console.log(response);
                expect(response.statusCode).to.equal(200);
                done();
            });
        });

        // it('content', function() {
        //   request('http://localhost:8080/' , function(error, response, body) {
        //     expect(body).to.equal('Hello World');
        //     });
        // });
    });


describe('quiz', function() {
    before(function(done){ //Before each test we empty the database
        Quiz.remove({}, (err) => { 
           done();         
        });     
    });

    it('/get quiz', function(done){

        request('http://localhost:3000/questionnaire/quiz', function(error, response, body) {
            expect(body).to.equal('[]');
            done();
        });
    });

    it('/post quiz', function(done){

        chai.request('http://localhost:3000').post('/questionnaire/quiz').send(sendData).end(function(error, response) {
            // console.log(response);
            expect(response.statusCode).to.equal(200);
            done();
        });
    });

    // it('/assert length of addition', function(done){

    //     request('http://localhost:3000/questionnaire/quiz', function(error, response, body) {
    //         expect(body).to.have.lengthOf(1);
    //         done();
    //     });
    // });
});
