const config = require('../config');

//Establish DB connection
const mongoose = require('mongoose');
mongoose.connect(config.MONGO_URI, { useMongoClient: true });

const db = mongoose.connection;
db.on('error', function () {
  console.error('Failed connecting with MongoDB. Is MongoDB up and running?');
});
db.once('open', function callback() {
  console.info('MongoDB connection is working:', config.MONGO_URI);
});

module.exports = {
  mongoose: mongoose,
  db: db
};

