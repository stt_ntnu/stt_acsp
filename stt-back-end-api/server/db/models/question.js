var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var answerSchema = new Schema(
    {
        answer: {type: String, required: true},
        correctness: {type: Number, required: true}
    }
);

var questionSchema = new Schema(
    {
        question: {type: String, required: true},
        code: {type: String, required: false},
        img: {data: Buffer, contentType: String, required: false},
        answer_explanation: {type: String, required: false},
        answers: {type: [answerSchema], required: true}
    }
);

module.exports = mongoose.model('Question', questionSchema);