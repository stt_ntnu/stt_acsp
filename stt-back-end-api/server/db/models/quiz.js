var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var questionSchema = require('./question');

//var categoryEnum = Object.freeze({ DataFlowTesting: 0, BasisPathTesting: 1, UnitTesting:2, WhiteBoxTesting: 4, BlackBoxTesting: 5, MultipleTestingMethods: 98, Other: 99});
//var categoryEnum = Object.freeze({ DataFlow: 'data flow testing', BasisPathTesting: 'basis path testing', UnitTesting: 'unit testing', WhiteBoxTesting: 'white box testing', BlackBoxTesting: 'black box testing', MultipleTestingMethods: 'multiple testing methods', Other: 'other'});


var categoryEnum = {
    values: ['data flow testing', 'basis path testing', 'unit testing', 'white-box testing', 'black-box testing', 'multiple testing methods', 'other'],
    message: 'The category field must follow a enum and the string `{VALUE}` is not valid.' 
}

var quizSchema = new Schema(
    {
        creator: {type: String, required: true},
        title: {type: String, required: true},
        description: {type: String, required: true},
        categories: [{type: String, required: true, enum: categoryEnum}],
        date_modified: {type: Number}, // Apparantly when a object is inserted the object's id contains the date it was inserted.
        questions: {
            type: [questionSchema.schema], 
            required: true
        }
    }
);

function minimum(value){
    return value >= 1;
}

module.exports = mongoose.model('Quiz', quizSchema);
