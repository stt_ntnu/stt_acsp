var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var discussionSchema = require('./discussion');

var forumSchema = new Schema(
    {
        question: {type: String, required: true},
        description: {type: String, required: true},
        img: {type: String, required: true},
        name: {type: String, required: true},
        email: {type: String, required: true},
        tags: [{type: String, required: true}],
        discussion: {type: [discussionSchema.schema], required: false}
    },{timestamps: true}
);

module.exports = mongoose.model('Forum', forumSchema);