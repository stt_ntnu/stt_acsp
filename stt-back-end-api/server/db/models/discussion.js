var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var discussionSchema = new Schema(
    {
        name: {type: String, required: true},
        img: {type: String, required: true},
        email: {type: String, required: true},
        answer: {type: String, required: true}
    },{timestamps: true}
);

module.exports = mongoose.model('Discussion', discussionSchema);