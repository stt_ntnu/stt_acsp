module.exports = {
    /**
     * In order to connect to DB locally instead which may be usefull if deployed on a server 
     * under our controll.
     * Download, install & start mongoDB to the server system
     * Also download, install & start MongoBooster for a GUI to interact with the DB
     * Start it & create a database and provide a anternaive URI to the DB
     * 
     * As it is now a development server hosts the DB at mlab.com, 
     * which is not intended for a production environment as it is now since it runs in a sandbox mode.
     * It may work for the intended use, but it is an uncertanty.
     * They offer paid alternatives that may be more suited for a production enviroments.
     */
    MONGO_URI: process.env.MONGO_URI || 'mongodb://stt_development:stt_development@ds149954.mlab.com:49954/stt',
    PORT: '3000'
  };