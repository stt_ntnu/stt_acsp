const config = require('./config');
var jqu = require('./jsonQuizUtil');
var fs = require('fs');

var imgPath = './assets/100x100.png';
var imageData = fs.readFileSync(imgPath);

//Establish DB connection
const mongoose = require('mongoose');
mongoose.connect(config.MONGO_URI, { useMongoClient: true });

const db = mongoose.connection;
db.on('error', function () {
  console.error('Failed connecting with MongoDB. Is MongoDB up and running?');
});
db.once('open', function callback() {
  console.info('MongoDB connection is working:', config.MONGO_URI);
});

const Question = require('../models/question');
const Quiz = require('../models/quiz')

module.exports = {mongoose, 
  db,

  /**
   * Adds a couple of quizzes to the DB
   * THe quizzes are defined by two different ways.
   */
  addTestData: function(){
    var question1 = new Question({
      question: 'What is 12x21?',
      // Optionally add a link to a image with: image_path: <path-to-image> ,
      answers: [
        {answer: '1221', correctness: 0},
        {answer: '252', correctness: 1},
        {answer: '135', correctness: 0}
      ],
      answer_explanation: 'Perform simple multiplication...'
    });
    
    var question2 = new Question({
      question: 'What is a apex point?',
      // Optionally add a link to a image with: image_path: <path-to-image> ,
      img: {data: imageData, contentType: 'image/png'},
      answers: [
        {answer: 'A wise point disproving another point in a discussion', correctness: 0},
        {answer: 'The most centered point in a geometrical shape', correctness: 0},
        {answer: 'The highest point in a geometrical shape', correctness: 1}
      ],
      answer_explanation: 'It\'s a mathematical term used in geometry, which indicates the highest point or the peak in a structure'
    });
    
    var date = new Date();
    
    
    var quiz1 = new Quiz(
      {
        creator: 'Mr. Smith',
        title: 'Random questions',
        description: 'A couple of questions testing your knowlege on mathematics',
        categories: ['data flow testing', 'white-box testing'],
        questions: [question1, question2]
      }
    )
    
    quiz1.save(function (err) {
      if(err){
        console.log('Erroneously response from DB, quiz not saved');
        console.log(err);
      } else {
        console.log('Succesfully saved quiz 1 to DB');
      }
    });
    
    
    //Test quiz utils
    var question3 = jqu.addQuestion('What are booleans?', null, "They are a value set to either true or false.",
    jqu.addAnswer('A variable that can be any number', 0),
    jqu.addAnswer('A datatype set to either the value true or false', 1),
    jqu.addAnswer('A lean ghost that says boo', 0)
    );
    
    var question4 = jqu.addQuestion('What are programming good for?', null, 'Everything',
    jqu.addAnswer('No idea', 0),
    jqu.addAnswer('Almost everything in some way', 1),
    jqu.addAnswer('Taking a jog', 0)
    );
    
    
    quiz2 = jqu.addQuiz('Mr. Smith', 'Basic CS', 'A quiz about basic computer science',
    jqu.addCategories('unit testing'),
    question3,
    question4);
    
    quiz2.save(function (err) {
      if(err){
        console.log('Erroneously response from DB, quiz not saved');
        console.log(err);
      } else {
        console.log('Succesfully saved quiz 2 to DB');
      }
    });
  },

  getQuizzes: function(){
      return db.collection('quizzes');

      
    
  }

  
};

