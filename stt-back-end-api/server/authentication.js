const jwt = require('express-jwt');
const jwksRsa = require('jwks-rsa');
const jwtAuthz = require('express-jwt-authz');

/**
 * Configuring the guard feature to use the permissionProperty namespace provided within the request's user field.
 * This is used to authenticate users based upon roles.
 * Usually used in cojunction with checkJwt
 * Roles possible: admin, teacher, student and user.
 * Ex usage: auth.guard.check('admin')
 * Use tha above in a router get, post or update method's parameters to secure access rights by role
 */
var guard = require('express-jwt-permissions')({
    permissionsProperty: 'https://stt.ntnu.no/roles'
});

/**
 * Configuration for validating the JWT token, if valid the user will ba able to access resources
 * secured with checkJwt in the parameters.
 */
const checkJwt = jwt({
    secret: jwksRsa.expressJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: `https://stt.eu.auth0.com/.well-known/jwks.json`
    }),

    audience: 'https://bitbucket.org/stt_ntnu/stt_acsp/',
    issuer: `https://stt.eu.auth0.com/`,
    algorithms: ['RS256']
});

module.exports = {
  guard: guard,
  checkJwt: checkJwt
}
