var mongoose = require('mongoose');

const Quiz = require('./db/models/quiz');
const Question = require('./db/models/question');

module.exports = {
    
      /**
      * This function will create a new quiz json object
      * The parameters are must be provided as this:
      * 1st: string value with quiz creators name
      * 2nd: string value with quiz title
      * 3rd: string value with quiz description
      * 4th and up: questionObject provided with addQuestion() method
      */
      addQuiz: function ()  {
        var newQuiz = { creator: null, title: null, description: null, categories: null, questions: [] };
        newQuiz.creator = arguments[0];
        newQuiz.title = arguments[1];
        newQuiz.description = arguments[2];
        newQuiz.categories = arguments[3];

        var offset = 4;
        for(var i = offset; i < arguments.length; i++){
          newQuiz.questions[i-offset] = arguments[i];
        }
      
        return new Quiz(newQuiz);
      },
    
      /**
      * The function's parameters take a variable number of arguments and every argument 
      * is a string with a category associated with the quiz.
      */
      addCategories: function () {
        return Array.prototype.slice.call(arguments);
      },
    
      /**
      * The function's parameters take a variable number of arguments:
      * 1st: must be a String with the question itselt,
      * 2nd: must be either null or a valid path to a image,
      * 3rd: must be either null or a explanation of the question
      * then the following parameters must be Objects generated with the addAnswer() method.
      */
      addQuestion: function () {
        var newQuestion = { question: null, image_path: null, answer_explanation: null, answers: [] };
        newQuestion.question = arguments[0];
        newQuestion.image_path = arguments[1];
        newQuestion.answer_explanation = arguments[2];

        var offset = 3;
        for(var i = offset; i < arguments.length; i++){
          newQuestion.answers[i-offset] = arguments[i];
        }
    
        return new Question(newQuestion);
      },
    
      /**
      * Creates a answer object, this function should be used in every parameter except the first of the addQuestion() method 
      * @param {String} answer must be a string
      * @param {Number} correctness must be a integer
      */
      addAnswer: function (answer, correctness) {
        var result = { answer: null, correctness: null };
        result.answer = answer;
        result.correctness = correctness;
        return result;
      }
    };